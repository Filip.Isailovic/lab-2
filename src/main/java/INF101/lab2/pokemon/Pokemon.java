package INF101.lab2.pokemon;

import java.lang.Math;
import java.util.Random;

public class Pokemon implements IPokemon {

    public String getName() {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    @Override
    public int getStrength() {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    @Override
    public int getCurrentHP() {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    @Override
    public int getMaxHP() {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public boolean isAlive() {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    @Override
    public void attack(IPokemon target) {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    @Override
    public void damage(int damageTaken) {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    @Override
    public String toString() {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

}
